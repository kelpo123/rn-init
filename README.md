# React Native Init

This project was bootstrapped with [Create React Native App](https://github.com/facebook/react-native).

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npx react native link`

### `cd ios && pod install`

Install all depedencies needed.<br />

If you want to run in android:

### `npx react-native run-android`

If you want to run in android:

### `npx react-native run-ios`

The apps will reload if you make edits.<br />
