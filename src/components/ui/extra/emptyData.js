import React, {memo} from 'react';
import {UITypography, UIIconFeather} from 'components/ui';
import {View, StyleSheet} from 'react-native';
const ThemedEmptyData = (props) => {
  const {style, ...restProps} = props;
  const {title, middle, cover} = restProps;
  const layout = middle ? styles.middle : styles.center;
  const heightLayout = cover ? styles.cover : null;
  return (
    <View style={[layout, heightLayout]}>
      <UIIconFeather name="inbox" size={40} />
      {title ? <UITypography title={title} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  middle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  cover: {
    paddingVertical: 50,
  },
});

export default memo(ThemedEmptyData);
