import React, {memo} from 'react';
import {UITypography} from 'components/ui';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import {setColor} from '../global-func';
const ThemedLoading = (props) => {
  const {style, ...restProps} = props;
  const {title, middle, cover, color} = restProps;
  const layout = middle ? styles.middle : styles.center;
  const heightLayout = cover ? styles.cover : null;
  return (
    <View style={[style, layout, heightLayout]}>
      <ActivityIndicator {...restProps} color={setColor(color)} />
      {title ? <UITypography title={title} top={10} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  middle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  cover: {
    paddingVertical: 50,
  },
});

export default memo(ThemedLoading);
