import React, {memo} from 'react';
import {useTheme} from '@ui-kitten/components';
import {StyleSheet, View} from 'react-native';
import {UITypography} from 'components/ui';
const ThemedBadge = (props) => {
  const theme = useTheme();
  const {themedStyle, style, ...rest} = props;
  return (
    <View
      {...rest}
      style={[styles.badge, {backgroundColor: theme['color-danger-500']}]}>
      <UITypography color="white" size={10} title="1" />
    </View>
  );
};
const styles = StyleSheet.create({
  badge: {
    width: 16,
    height: 16,
    borderRadius: 23,
    lineHeight: 17,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 2,
    right: -5,
  },
});
export default memo(ThemedBadge);
