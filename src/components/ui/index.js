import React from 'react';
import ThemedLoading from './extra/loading';
import ThemedBadge from './extra/badge';
import ThemedEmptyData from './extra/emptyData';
import ThemedTypography from './typography';
import ThemedContainer from './basic/container';
import ThemedRow from './basic/row';
import ThemedColumn from './basic/column';
import ThemedTouchable from './basic/touchable';

//extra
export const UILoading = (props) => <ThemedLoading {...props} />;
export const UIEmptyData = (props) => <ThemedEmptyData {...props} />;
export const UIBadge = (props) => <ThemedBadge {...props} />;

//typography
export const UITypography = (props) => <ThemedTypography {...props} />;

//basic
export const UIContainer = (props) => <ThemedContainer {...props} />;
export const UIRow = (props) => <ThemedRow {...props} />;
export const UIColumn = (props) => <ThemedColumn {...props} />;
export const UITouchable = (props) => <ThemedTouchable {...props} />;
