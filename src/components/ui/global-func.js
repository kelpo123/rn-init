import {useTheme} from '@ui-kitten/components';
const setColor = (color) => {
  const theme = useTheme();
  switch (color) {
    case 'white':
      return theme['color-white'];
    case 'primary':
      return theme['color-primary-500'];
    case 'soft-black':
      return theme['color-primary-400'];
    case 'red':
      return theme['color-danger-500'];
    default:
      return theme['color-black'];
  }
};

const setTypography = (typo) => {
  switch (typo) {
    case 'light':
      return 'Gotham-Light';
    case 'book':
      return 'Gotham-Book';
    case 'bold':
      return 'Gotham-Bold';
    case 'ultra':
      return 'Gotham-Ultra';
    case 'narrow-black':
      return 'GothamNarrow-Black';
    default:
      return 'Gotham-Medium';
  }
};

const setAlign = (type) => {
  switch (type) {
    case 'center':
      return 'center';
    case 'flex-end':
      return 'flex-end';
    default:
      return 'flex-start';
  }
};

const setJustify = (type) => {
  switch (type) {
    case 'flex-start':
      return 'flex-start';
    case 'flex-end':
      return 'flex-end';
    default:
      return 'center';
  }
};

const setMargin = (props) => {
  const {top, left, right, bot} = props;
  const margin = {
    marginTop: top,
    marginBottom: bot,
    marginRight: right,
    marginLeft: left,
  };
  return margin;
};

const twoColumnFormat = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);
  let newArr = data;

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    newArr.push({key: `blank-${numberOfElementsLastRow}`, type: 'empty'});
    numberOfElementsLastRow = numberOfElementsLastRow + 1;
  }

  return newArr;
};

export {
  setColor,
  setTypography,
  setAlign,
  setJustify,
  setMargin,
  twoColumnFormat,
};
