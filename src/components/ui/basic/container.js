import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';
import {setAlign, setMargin, setJustify} from '../global-func';
const ThemedContainer = (props) => {
  const {children, align, justify, flex, style, ...rest} = props;
  return (
    <View
      {...rest}
      style={[
        style,
        styles.container,
        setMargin(props),
        {
          alignItems: setAlign(align),
          justifyContent: setJustify(justify),
          flex,
        },
      ]}>
      {children}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
});

export default memo(ThemedContainer);
