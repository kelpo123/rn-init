import React from 'react';
import {Provider} from 'react-redux';
import {store, persistor} from 'reduxjs/store';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {FeatherIconsPack} from './app-icons-feather';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {AppNavigator} from 'navigations/app.navigator';
import {mapping, light as lightTheme} from '@eva-design/eva';
import {default as appTheme} from './app-theme.json';
import FlashMessage from 'react-native-flash-message';
import {enableScreens} from 'react-native-screens';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';

const theme = {...lightTheme, ...appTheme};

enableScreens();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <IconRegistry icons={[EvaIconsPack, FeatherIconsPack]} />
        <ApplicationProvider mapping={mapping} theme={theme}>
          <SafeAreaProvider>
            <SafeAreaView style={styles.wrapper}>
              <StatusBar barStyle="dark-content" />
              <AppNavigator />
              <FlashMessage position="top" duration={1500} />
            </SafeAreaView>
          </SafeAreaProvider>
        </ApplicationProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
});
