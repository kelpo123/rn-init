import React from 'react';

export type Task = () => {};

export const LoadFontsTask = (fonts) => {
  const message = [
    'There is no need to use this task in Bare RN Project.',
    'Use `react-native.config.js',
    'Documentation: https://github.com/react-native-community/cli/blob/master/docs/configuration.md',
  ].join('\n');

  console.warn(message);

  return Promise.resolve(null);
};

export const LoadAssetsTask = () => {
  const message = [
    'There is no need to use this task in Bare RN Project.',
    'Use `react-native.config.js',
    'Documentation: https://github.com/react-native-community/cli/blob/master/docs/configuration.md',
  ].join('\n');

  console.warn(message);

  return Promise.resolve(null);
};

/**
 * Loads application configuration and returns content of the application when done.
 *
 * @property {Task[]} tasks - Array of tasks to prepare application before it's loaded.
 * A single task should return a Promise with value and a by which this value is accessible.
 *
 * @property {any} fallback - Fallback configuration that is used as default application configuration.
 * May be useful at first run.
 *
 * @property {(props: { loaded: boolean }) => React.ReactElement} placeholder - Element to render
 * while application is loading.
 *
 * @property {(result: any) => React.ReactElement} children - Should return Application component
 */
export const AppLoading = (props) => {
  const [loading, setLoading] = true;
  const loadingResult = props.initialConfig || {};

  const onTasksFinish = () => {
    setLoading(false);
  };

  React.useEffect(() => {
    if (loading) {
      startTasks().then(onTasksFinish);
    }
  }, [loading]);

  const saveTaskResult = (result) => {
    if (result) {
      loadingResult[result[0]] = result[1];
    }
  };

  const createRunnableTask = (task: Task) => {
    return task().then(saveTaskResult);
  };

  const startTasks = async () => {
    if (props.tasks) {
      return Promise.all(props.tasks.map(createRunnableTask));
    }
    return Promise.resolve();
  };

  return (
    <React.Fragment>
      {!loading && props.children(loadingResult)}
      {props.placeholder && props.placeholder({loading})}
    </React.Fragment>
  );
};
