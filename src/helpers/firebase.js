import firebase from 'react-native-firebase';

export function loginPhone(number) {
  return firebase.auth().signInWithPhoneNumber(number);
}

export function getTokenFirebase() {
  return firebase.auth().currentUser.getIdToken(true);
}

export function getFcmToken() {
  return firebase.messaging().getToken();
}
