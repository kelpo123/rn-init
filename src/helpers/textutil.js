import moment from 'moment';

const formatDateDefault = [
  'YYYY-MM-DD HH:mm:ss',
  'DD-MM-YYYY HH:mm:ss',
  'YYYY-MM-DD',
  'DD-MM-YYYY',
];

class TextUtil {
  strReplace = (source, replace, replaceWith) => {
    var value = source;
    var i = 0;
    for (i; i < value.length; i++) {
      value = value.replace(replace, replaceWith);
    }
    return value;
  };

  upperCaseString = i => {
    if (typeof i === 'string') {
      return i.toUpperCase();
    }
    return i;
  };

  formattingNumber = i => {
    if (typeof i === 'number') {
      return i.toLocaleString(navigator.language, {minimumFractionDigits: 0});
    }
    return i;
  };

  validateEmail = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  };

  getListYear() {
    var currentYear = new Date().getFullYear(),
      years = [];
    var startYear = 2000;
    while (startYear <= currentYear) {
      years.push(startYear++);
    }
    return years;
  }

  getDate(value, newFormat = null) {
    if (newFormat == null) {
      newFormat = 'DD MMMM YYYY';
    }
    let date = moment(value, formatDateDefault).format(newFormat);
    return date;
  }

  getCurrentDate() {
    let date = moment(new Date(), 'DD-MM-YYYY').format();
    return date;
  }

  getDateTime(value) {
    let date = moment(value, formatDateDefault).format('DD MMMM YYYY HH:mm:ss');
    return date;
  }

  getDateTime2(value) {
    let date = moment(value, formatDateDefault).format('DD MMMM YYYY HH:mm');
    return date;
  }

  getFullDay(value) {
    let date = moment(value, formatDateDefault).format('dddd, DD MMMM YYYY');
    return date;
  }

  getTime(value) {
    let date = moment(value, formatDateDefault).format('HH:mm:ss');
    return date;
  }

  getHour(value) {
    let date = moment(value, formatDateDefault).format('HH:mm');
    return date;
  }

  formatMoney(num) {
    num = num + '';

    if (num == '' || num == '0') return '';

    num = num.replace(/\./g, '');
    var num_parts = num.toString().split('.');
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    return num_parts.join('.');
  }

  moneytoInt(num) {
    if (num) {
      return num.replace(/\./g, '');
    } else {
      return 0;
    }
  }

  blurName(name) {
    name = name.slice(0, -3);
    name = name + '***';
    return name;
  }

  firstCapital(str) {
    return str.replace(/^\w/, c => c.toUpperCase());
  }

  timeSince(date) {
    const FORMAT = 'DD-MM-yyyy HH:mm:ss';
    const theDate = moment(date, FORMAT);

    var seconds = Math.floor((new Date() - theDate) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
      return interval + ' years ago';
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + ' months ago';
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + ' days ago';
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + ' hours ago';
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + ' minutes ago';
    }
    return Math.floor(seconds) + ' seconds ago';
  }
}

export default new TextUtil();
