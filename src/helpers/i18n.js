import I18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';

import id from '../locales/id.json';
import en from '../locales/en.json';

const locales = RNLocalize.getLocales();

if (Array.isArray(locales)) {
  I18n.locale = locales[0].languageTag;
}

I18n.fallbacks = true;
I18n.translations = {
  id,
  en,
};

export default I18n;
