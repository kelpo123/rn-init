import axios from 'axios';
import handleError from 'helpers/handleError';
import {BASE_URL_API, TIME_OUT, APP_KEY} from 'react-native-dotenv';
import AsyncStorage from '@react-native-community/async-storage';
import UserAgent from 'react-native-user-agent';
const getInstance = (time) => {
  const instance = axios.create({
    baseURL: BASE_URL_API,
    timeout: time ? parseInt(TIME_OUT) || 10000 : 99999999999,
    headers: {
      'Content-Type': 'application/json',
      'User-Agent': UserAgent.getUserAgent(),
    },
  });
  instance.interceptors.request.use(
    async (config) => {
      if (
        config.method == 'get' ||
        config.method == 'delete' ||
        config.data == undefined
      ) {
        config.data = true;
      }
      const newConfig = config;
      const token = await AsyncStorage.getItem(APP_KEY);
      if (token) {
        newConfig.headers['X-AUTH-TOKEN'] = token;
      }
      console.log(newConfig);
      return newConfig;
    },
    (error) => Promise.reject(error),
  );
  return instance;
};

const httpRequest = (method, path, data, headers = {}, timeout = true) => {
  return new Promise((resolve, reject) => {
    getInstance(timeout)
      [method](path, data, headers)
      .then((response) => resolve(response))
      .catch((error) => reject(handleError(error)));
  });
};

const httpRequestNoError = (
  method,
  path,
  data,
  headers = {},
  timeout = true,
) => {
  return new Promise((resolve) => {
    getInstance(timeout)
      [method](path, data, headers)
      .then((response) => resolve(response));
  });
};

export default {
  get(path, data, headers) {
    return httpRequest('get', path, data, headers);
  },
  getNoError(path, data, headers) {
    return httpRequestNoError('get', path, data, headers);
  },
  post(path, data, headers) {
    return httpRequest('post', path, data, headers);
  },
  postNoTimeout(path, data, headers) {
    return httpRequest('post', path, data, headers, false);
  },
  put(path, data, headers) {
    return httpRequest('put', path, data, headers);
  },
  delete(path, data, headers) {
    return httpRequest('delete', path, data, headers);
  },
  patch(path, data, headers) {
    return httpRequest('patch', path, data, headers);
  },
  patchNoTimeout(path, data, headers) {
    return httpRequest('patch', path, data, headers, false);
  },
};
