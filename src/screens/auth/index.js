import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {UIContainer, UITypography} from 'components/ui';
import {Button} from '@ui-kitten/components';
export default function Auth() {
  const t = useSelector((state) => state.setting.locale);
  const [counter, setCounter] = useState(1);
  return (
    <UIContainer flex={1} align="center" top={10}>
      <UITypography title={t('auth.signin.title')} size={26} type="bold" />
      <Button onPress={() => setCounter(counter + 1)}>
        {t('auth.signin.submit')} {counter}
      </Button>
    </UIContainer>
  );
}
