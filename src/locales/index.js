import i18n from 'i18n-js';
import {LANGUAGE} from '@env';

// Import all locales
import en from './en.json';
import id from './id.json';

i18n.translations = {
  en,
  id,
};
i18n.locale = LANGUAGE;

export function strings(name, params = {}) {
  return i18n.t(name, params);
}

export default i18n;
