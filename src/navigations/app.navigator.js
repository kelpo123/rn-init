// import 'react-native-gesture-handler';
import React from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {AuthNavigator} from './auth.navigator';
import {TabNavigator} from './tab.navigator';
import {LogBox} from 'react-native';
LogBox.ignoreAllLogs();

const navigatorTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    // prevent layout blinking when performing navigation
    background: 'transparent',
  },
};

export const AppNavigator = () => {
  const token = useSelector((state) => state.user?.data?.token);
  return (
    <NavigationContainer theme={navigatorTheme}>
      {!token ? <AuthNavigator /> : <TabNavigator />}
    </NavigationContainer>
  );
};
