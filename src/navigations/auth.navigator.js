import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SignInScreen from 'screens/auth';
import SignUpScreen from 'screens/auth/signUp';
const Stack = createStackNavigator();

export const AuthNavigator = () => {
  return (
    <>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="SignIn" component={SignInScreen} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
      </Stack.Navigator>
    </>
  );
};
