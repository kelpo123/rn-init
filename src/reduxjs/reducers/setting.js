import {strings} from 'locales/index';
const initialState = {
  setting: {
    locale: strings,
  },
};

const setting = (state = initialState.setting, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
export default setting;
