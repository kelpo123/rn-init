import {compose, createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import rootReducer from '../reducers';
import AsyncStorage from '@react-native-community/async-storage';

const loggerMiddleware = createLogger();
const composeEnhancers = compose;

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth'],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
let middleware = [];
if (__DEV__) {
  middleware = [thunkMiddleware, loggerMiddleware];
} else {
  middleware = [thunkMiddleware];
}

export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(...middleware)),
);
export const persistor = persistStore(store);
